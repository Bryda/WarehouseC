﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Model.Models;

namespace Warehouse.ViewModels
{
    public class EditProductViewModel
    {     
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Product name is required!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Quantity field is required!")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Price field is required!")]
        [RegularExpression(@"((\d)*[,]\d\d)",ErrorMessage = "Price format is 123,45.")]
        public double Price { get; set; }

        [Display(Name = "Category")]
        public string CategoryName { get; set; }

        [Display(Name = "Categories")]
        [Required(ErrorMessage = "Category is required!")]
        public SelectList CategoryList { get; set; } 
    }
}