﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NLog;
using Warehouse.Data.DataBase;
using Warehouse.Model.Models;
using Warehouse.Service;
using Warehouse.Service.InterfacesServices;
using Warehouse.Service.Services;
using Warehouse.ViewModels;

namespace Warehouse.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private ICustomerService _customerService;
        private IProductService _productService;

        public CustomerController(ICustomerService customerService, IProductService productService)
        {
            _customerService = customerService;
            _productService = productService;
        }

        public ActionResult CustomersList()
        {
            var customers = _customerService.GetAll();
            return View(customers);
        }

        public ActionResult DetailsCustomer(int id)
        {
            Customer customer = _customerService.FindById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            AddProductsViewModel model = new AddProductsViewModel()
            {
                CustomerId = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                CustomerProducts = _customerService.FindAllCustomerProducts(id).ToList()
            };
            return View(model);
        }

        public ActionResult CreateCustomer()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCustomer([Bind(Include = "Id,FirstName,LastName")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                _customerService.Create(customer);
                _logger.Info("Created new customer: {0} {1}", customer.FirstName, customer.LastName);
                return RedirectToAction("CustomersList");
            }

            return View(customer);
        }

        public ActionResult EditCustomer(int id)
        {
            Customer customer = _customerService.FindById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCustomer([Bind(Include = "Id,FirstName,LastName")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                _customerService.Edit(customer);
                _logger.Info("Customer {0} {1} was edited", customer.FirstName, customer.LastName);
                return RedirectToAction("CustomersList");
            }
            return View(customer);
        }

        public ActionResult DeleteCustomer(int id)
        {
            Customer customer = _customerService.FindById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        [HttpPost, ActionName("DeleteCustomer")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Customer customer = _customerService.FindById(id);
            _customerService.Delete(customer);
            _logger.Info("Deleted customer: {0} {1}", customer.FirstName, customer.LastName);
            return RedirectToAction("CustomersList");
        }

        public ActionResult AddProducts(int id)
        {
            Customer customer = _customerService.FindById(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            AddProductsViewModel model = new AddProductsViewModel()
            {
                CustomerId = id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                CustomerProducts = _customerService.FindAllCustomerProducts(id).ToList()
            };                        
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddProducts(AddProductsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var prod = _productService.Find(product => product.Name.Equals(model.Name));
                if (prod != null)
                {
                    if (prod.Quantity >= model.Quantity)
                    {
                        CustomerProduct customerProduct = new CustomerProduct()
                        {
                            CustomerId = model.CustomerId,
                            Name = model.Name,
                            Price = model.Price,
                            Quantity = model.Quantity
                        };
                        _customerService.SaveCustomerProduct(customerProduct);
                        prod.Quantity -= model.Quantity;
                        _productService.Edit(prod);
                    }
                    else
                    {
                        ViewBag.ToMuch = "Not enough products in stock!";
                    }                   
                }
                else
                {
                    ViewBag.NoProduct = "Product doesn't exist!";                    
                }
               
            }

            Customer customer = _customerService.FindById(model.CustomerId);
            if (customer == null)
            {
                return HttpNotFound();
            }
            AddProductsViewModel addModel = new AddProductsViewModel()
            {
                CustomerId = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                CustomerProducts = _customerService.FindAllCustomerProducts(customer.Id).ToList()
            };
            return View(addModel);
        }

        public ActionResult EditCustomerProduct(int id)
        {
            var customerProduct = _customerService.FindCustomerProduct(id);
            return View(customerProduct);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCustomerProduct(CustomerProduct customerProduct)
        {
            _customerService.EditCustomerProduct(customerProduct);
            return RedirectToAction("DetailsCustomer", new { id = customerProduct.CustomerId});
        }

        public ActionResult DeleteCustomerProduct(int id)
        {
            var product = _customerService.FindCustomerProduct(id);
            _customerService.DeleteCustomerProduct(product);
            _logger.Info("Product {0} {1} {2} was deleted", product.Customer.FirstName, product.Customer.LastName, product.Name);
            return RedirectToAction("DetailsCustomer", new {id = product.CustomerId});
        }

    }
}
