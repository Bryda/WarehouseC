﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using NLog;
using Warehouse.Data.DataBase;
using Warehouse.Data.Enums;
using Warehouse.ViewModels;

namespace Warehouse.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        private ApplicationDbContext context;

        public ManageController()
        {
            context = new ApplicationDbContext();
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            return View();
        }


#region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

#endregion

        public ActionResult ListUsers()
        {
            var roles = System.Enum.GetNames(typeof (RolesEnum)).ToList();
            List<UserViewModel> userVM= new List<UserViewModel>();
            roles.ForEach((role) =>
            {
                var roleDb = (from r in context.Roles where r.Name.Contains(role) select r).FirstOrDefault();
                var users = context.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(roleDb.Id)).ToList();

                var userList = users.Select(user => new UserViewModel
                {
                    UserId = user.Id,
                    Username = user.UserName,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    RoleName = role
                }).ToList();
                userVM.AddRange(userList);
            });

            return View(userVM);            
        }        

        public ActionResult EditUser(string userName)
        {
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            ApplicationUser user = userManager.FindByNameAsync(userName).Result;
            ViewBag.userName = user.UserName;
            ViewBag.Name = new SelectList(context.Roles.ToList(), "Name", "Name");
            return View(user);
        }

        private void ChangeUserRole(string userId, string userRole)
        {
            var UserManger = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var currentRoles = System.Enum.GetNames(typeof(RolesEnum)).ToList();
            currentRoles.ForEach(role => UserManger.RemoveFromRole(userId, userRole));
            UserManger.AddToRole(userId, userRole);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(ApplicationUser model)
        {
            if (ModelState.IsValid)
            {
                var store = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(store);
                ApplicationUser user = userManager.FindByNameAsync(model.UserName).Result;

                if (user != null)
                {
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Email = model.Email;
                    user.PhoneNumber = model.PhoneNumber;

                    if (User.IsInRole("Admin"))                    
                        if (Request.Params.Get("UserRole").IsNullOrWhiteSpace())
                        {
                            return RedirectToAction("EditUser", "Manage");
                        }
                        else
                        {
                            ChangeUserRole(user.Id, Request.Params.Get("UserRole"));
                        }                                            

                    context.Entry(user).State = EntityState.Modified;
                    context.SaveChanges();
                    _logger.Info("Edited user {0} {1}", user.FirstName, user.LastName);
                }

                if (User.IsInRole("Admin"))
                    return Redirect("/Manage/ListUsers");

                return Redirect("/Manage/Index");
            }
            return RedirectToAction("Index", "Manage");
        }

        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(string id)
        {            
            var user = UserManager.Users.SingleOrDefault(u => u.Id == id);

            await UserManager.DeleteAsync(user);
            _logger.Info("Deleted user {0} {1}", user.FirstName, user.LastName);
            return RedirectToAction("ListUsers", "Manage");
        }

        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.RemovePasswordAsync(user.Id);
            result = await UserManager.AddPasswordAsync(user.Id, model.Password);
            if (result.Succeeded)
            {
                _logger.Info("Reset user password for {0} {1}", user.FirstName, user.LastName);
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }
    }
}