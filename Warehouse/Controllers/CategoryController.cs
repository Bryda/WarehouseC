﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NLog;
using Warehouse.Data.DataBase;
using Warehouse.Model.Models;
using Warehouse.Service.InterfacesServices;
using Warehouse.Service.Services;

namespace Warehouse.Controllers
{
    public class CategoryController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public ActionResult CategoriesList()
        {
            var categoriesList = _categoryService.GetAll();
            return View(categoriesList);
        }

        public ActionResult CreateCategory()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCategory([Bind(Include = "Id,Name")] Category category)
        {
            if (ModelState.IsValid)
            {
                _categoryService.Create(category);     
                logger.Info("Created new category:{0}", category.Name);           
                return RedirectToAction("CategoriesList");
            }

            return View(category);
        }
        
        public ActionResult EditCategory(int id)
        {
            Category category = _categoryService.FindById(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCategory([Bind(Include = "Id,Name")] Category category)
        {
            if (ModelState.IsValid)
            {
                _categoryService.Edit(category);
                logger.Info("Category {0} was edited", category.Name);
                return RedirectToAction("CategoriesList");
            }
            return View(category);
        }
        
        public ActionResult DeleteCategory(int id)
        {
            Category category = _categoryService.FindById(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }
        
        [HttpPost, ActionName("DeleteCategory")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Category category = _categoryService.FindById(id);
            _categoryService.Delete(category);
            logger.Info("Deleted category: {0}", category.Name);
            return RedirectToAction("CategoriesList");
        }

    }
}
