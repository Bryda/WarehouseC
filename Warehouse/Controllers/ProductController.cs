﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLog;
using Warehouse.Data.Infrastructure;
using Warehouse.Data.Infstracture;
using Warehouse.Data.Repositories;
using Warehouse.Model.Models;
using Warehouse.Service;
using Warehouse.Service.InterfacesServices;
using Warehouse.Service.Services;
using Warehouse.ViewModels;

namespace Warehouse.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private IProductService _productService;
        private ICategoryService _categoryService;

        public ProductController(IProductService productService, ICategoryService categoryService)
        {
            _productService = productService;
            _categoryService = categoryService;
        }

        // GET: Product
        public ActionResult ProductsList()
        {
            var products = _productService.GetAll().ToList();
            return View(products);
        }

        public ActionResult EditProduct(int id)
        {
            var product = _productService.FindById(id);
            var editProduct = new EditProductViewModel()
            {
                Name = product.Name,
                Price = product.Price,
                Quantity = product.Quantity,
                CategoryName = product.Category.Name,
                CategoryList = new SelectList(_categoryService.GetAll().ToList(), "Name", "Name")
            };
            return View(editProduct);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProduct(EditProductViewModel product)
        {
            var saveProduct = new Product()
            {
                Id = product.Id,
                Name = product.Name,
                Price = product.Price,
                Quantity = product.Quantity,
                CategoryId = _categoryService.FindCategoryByName(Request.Params.Get("Category")).Id,
            };
            _productService.Edit(saveProduct);
            return RedirectToAction("ProductsList", "Product");
        }

        public ActionResult DeleteProduct(int id)
        {
            var product = _productService.FindById(id);
            _productService.Delete(product);
            _logger.Info("Deleted product {0}", product.Name);
            return RedirectToAction("ProductsList", "Product");
        }

        public ActionResult CreateProduct()
        {
            ViewBag.categoriesList = new SelectList(_categoryService.GetAll().ToList(),"Name","Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProduct(Product product)
        {            
            if (ModelState.IsValid)
            {
                product.CategoryId = _categoryService.FindCategoryByName(Request.Params.Get("CategoryList")).Id;
                _productService.Create(product);
                _logger.Info("Created product {0}", product.Name);
                return RedirectToAction("ProductsList");
            }
            ViewBag.categoriesList = new SelectList(_categoryService.GetAll().ToList(), "Name", "Name");
            return View(product);
        }
    }
}