﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Model.Models;

namespace Warehouse.Service.InterfacesServices
{
     public interface ICategoryService : IService<Category>
     {
         Category FindCategoryByName(string name);
     }
}
