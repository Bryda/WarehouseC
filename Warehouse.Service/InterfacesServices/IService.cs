﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Service.InterfacesServices
{
    public interface IService<T> where T : class
    {
        void Create(T entity);

        void Edit(T entity);

        void Delete(T entity);

        void Delete(Expression<Func<T, bool>> where);

        T FindById(int id);

        T Find(Expression<Func<T, bool>> where);

        IEnumerable<T> GetAll();

        IEnumerable<T> GetMany(Expression<Func<T, bool>> where);
    }
}
