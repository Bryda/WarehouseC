﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Data.Infstracture;
using Warehouse.Data.Repositories;
using Warehouse.Service.InterfacesServices;

namespace Warehouse.Service.Services
{
    public class Service<T>: IService<T> where T : class
    {
        protected IRepository<T> Repository;

        public Service()
        {
            Repository = new Repository<T>();
        }


        public void Create(T entity)
        {
            Repository.Add(entity);
        }

        public void Edit(T entity)
        {
            Repository.Update(entity);
        }

        public void Delete(T entity)
        {
            Repository.Delete(entity);
        }

        public void Delete(Expression<Func<T, bool>> @where)
        {
            Repository.Delete(where);
        }

        public T FindById(int id)
        {
            return Repository.GetById(id);
        }

        public T Find(Expression<Func<T, bool>> @where)
        {
            return Repository.Get(where);
        }

        public IEnumerable<T> GetAll()
        {
            return Repository.GetAll();
        }

        public IEnumerable<T> GetMany(Expression<Func<T, bool>> @where)
        {
            return Repository.GetMany(where);
        }
    }
}
