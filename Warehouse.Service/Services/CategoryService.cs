﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Data.Infrastructure;
using Warehouse.Data.Repositories;
using Warehouse.Model.Models;
using Warehouse.Service.InterfacesServices;

namespace Warehouse.Service.Services
{
    public class CategoryService: Service<Category>, ICategoryService
    {
        private ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public Category FindCategoryByName(string name)
        {
            return _categoryRepository.findByName(name);
        }
    }
}
