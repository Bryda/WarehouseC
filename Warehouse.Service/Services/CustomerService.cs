﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Data.Infrastructure;
using Warehouse.Data.Repositories;
using Warehouse.Model.Models;
using Warehouse.Service.InterfacesServices;

namespace Warehouse.Service.Services
{
    public class CustomerService:Service<Customer>,ICustomerService
    {
        private ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public IEnumerable<CustomerProduct> FindAllCustomerProducts(int customerId)
        {
            return _customerRepository.GetCustomerProducts(customerId);
        }

        public void SaveCustomerProduct(CustomerProduct customerProduct)
        {
            _customerRepository.SaveCustomerProduct(customerProduct);
        }

        public CustomerProduct FindCustomerProduct(int id)
        {
            return _customerRepository.GetCustomerProduct(id);
        }

        public void EditCustomerProduct(CustomerProduct customerProduct)
        {
            _customerRepository.EditCustomerProduct(customerProduct);
        }

        public void DeleteCustomerProduct(CustomerProduct customerProduct)
        {
            _customerRepository.DeleteCustomerProduct(customerProduct);
        }
    }
}
