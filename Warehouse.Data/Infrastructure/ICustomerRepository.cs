﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Data.Infstracture;
using Warehouse.Model.Models;

namespace Warehouse.Data.Infrastructure
{
    public interface ICustomerRepository:IRepository<Customer>
    {
        IEnumerable<CustomerProduct> GetCustomerProducts(int customerId);
        void SaveCustomerProduct(CustomerProduct customerProduct);

        CustomerProduct GetCustomerProduct(int id);

        void EditCustomerProduct(CustomerProduct customerProduct);

        void DeleteCustomerProduct(CustomerProduct customerProduct);
    }
}
