﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Warehouse.Data.DataBase;
using Warehouse.Data.Infstracture;

namespace Warehouse.Data.Repositories
{
    public class Repository<T>: IRepository<T> where T : class
    {
        private Logger _logger = LogManager.GetCurrentClassLogger();
        protected ApplicationDbContext Context = new ApplicationDbContext();

        public void Add(T entity)
        {
            try
            {
                Context.Set<T>().Add(entity);
                Context.SaveChanges();
            }
            catch (Exception)
            {
                _logger.Error("Can not add: {0}", entity);
                throw;
            }

        }

        public void Update(T entity)
        {
            try
            {
                Context.Set<T>().Attach(entity);
                Context.Entry(entity).State = EntityState.Modified;
                Context.SaveChanges();
            }
            catch (Exception)
            {
                _logger.Warn("Can not update: {0}", entity);
                throw;
            }
            
        }

        public void Delete(T entity)
        {
            try
            {
                Context.Set<T>().Attach(entity);
                Context.Entry(entity).State = EntityState.Deleted;
                Context.SaveChanges();
            }
            catch (Exception)
            {
                _logger.Warn("Can not delete: {0}", entity);
                throw;
            }

        }

        public void Delete(Expression<Func<T, bool>> @where)
        {
                IEnumerable<T> objects = Context.Set<T>().Where<T>(where).AsEnumerable();
                foreach (T obj in objects)
                {
                try
                {
                    Context.Set<T>().Remove(obj);
                }
                catch (Exception)
                {
                    _logger.Warn("Can not delete: {0}", obj);
                    throw;
                }
            }
                
            
            
        }

        public T GetById(int id)
        {
            return Context.Set<T>().Find(id);
        }

        public T Get(Expression<Func<T, bool>> @where)
        {
            return Context.Set<T>().Where(where).FirstOrDefault();
        }

        public IEnumerable<T> GetAll()
        {
            return Context.Set<T>();
        }

        public IEnumerable<T> GetMany(Expression<Func<T, bool>> @where)
        {
            return Context.Set<T>().Where(where).ToList();
        }
    }
}
