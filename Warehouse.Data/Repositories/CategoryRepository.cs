﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Warehouse.Data.Infrastructure;
using Warehouse.Data.Infstracture;
using Warehouse.Model.Models;

namespace Warehouse.Data.Repositories
{
    public class CategoryRepository:Repository<Category>, ICategoryRepository
    {
        public Category findByName(string name)
        {
            return Context.Set<Category>().FirstOrDefault(category => category.Name.Equals(name));
        }
    }
}
