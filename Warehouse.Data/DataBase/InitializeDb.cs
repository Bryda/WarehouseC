﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Warehouse.Data.Enums;
using Warehouse.Model.Models;

namespace Warehouse.Data.DataBase
{
    public class InitializeDb : DropCreateDatabaseAlways<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            List<string> rolesList = System.Enum.GetNames(typeof (RolesEnum)).ToList();

            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            rolesList.ForEach(role =>
            {
                if (!RoleManager.RoleExists(role))
                {
                    RoleManager.Create(new IdentityRole(role));
                }
            });

            var UserManger = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var user = new ApplicationUser();
            user.UserName = "admin";
            user.Email = "admin@admin.com";
            user.FirstName = "Admin";
            user.LastName = "Admin";
            user.PhoneNumber = "123-123-123";

            string userPassword = "Admin123-";

            if (UserManger.Create(user, userPassword).Succeeded)            
                UserManger.AddToRole(user.Id, RolesEnum.Admin.ToString());            

            user = new ApplicationUser();
            user.UserName = "jkowalski";
            user.Email = "jan@kowalski.com";
            user.FirstName = "Jan";
            user.LastName = "Kowalski";
            user.PhoneNumber = "321-123-413";

            userPassword = "Zaq123-";

            if (UserManger.Create(user, userPassword).Succeeded)            
                UserManger.AddToRole(user.Id, RolesEnum.Employee.ToString());
            

            user = new ApplicationUser();
            user.UserName = "anowak";
            user.Email = "adam@nowak.com";
            user.FirstName = "Adam";
            user.LastName = "Nowak";
            user.PhoneNumber = "234-346-486";

            userPassword = "Zaq123-";

            if (UserManger.Create(user, userPassword).Succeeded)            
                UserManger.AddToRole(user.Id, RolesEnum.Employee.ToString());    
            
            var category = new Category();
            category.Name = "Kategoria 1";
            context.Categories.Add(category);
            context.SaveChanges();

            category = new Category();
            category.Name = "Kategoria 2";
            context.Categories.Add(category);
            context.SaveChanges();

            var product = new Product
            {
                Name = "klawiatura",
                Quantity = 500,
                Price = 99.99,
                CategoryId = 1
            };

            context.Products.Add(product);
            //context.SaveChanges();

            product = new Product();
            product.Name = "monitor";
            product.Quantity = 1201;
            product.Price = 999.99;
            product.CategoryId = 2;

            context.Products.Add(product);
            context.SaveChanges();

            var customer = new Customer()
            {
                FirstName = "Lorem",
                LastName = "Ipsum"
            };

            context.Customers.Add(customer);
            context.SaveChanges();

            customer = new Customer()
            {
                FirstName = "Dolor",
                LastName = "Sit"
            };

            context.Customers.Add(customer);
            context.SaveChanges();

            var customerProduct = new CustomerProduct()
            {
                CustomerId = 1,
                Name = "klawiatura",
                Quantity = 10,
                Price = 99.99                
            };

            context.CustomerProducts.Add(customerProduct);
            context.SaveChanges();

            customerProduct = new CustomerProduct()
            {
                CustomerId = 1,
                Name = "monitor",
                Quantity = 121,
                Price = 999.99
            };

            context.CustomerProducts.Add(customerProduct);
            context.SaveChanges();

        }
    }
}