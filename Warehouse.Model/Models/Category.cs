using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Warehouse.Model.Models
{
    public class Category
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Field is required!")]
        public string Name { get; set; }
        
        public virtual ICollection<Product> Products { get; set; }
    }
}