﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Model.Models
{
    public class Customer
    {
        public int Id { get; set; }

        [Display(Name = "First name")]
        [Required(ErrorMessage = "Field is required!")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required(ErrorMessage = "Field is required!")]
        public string LastName { get; set; }

        public virtual ICollection<CustomerProduct> CustomerProducts { get; set; }
    }
}
