using System.ComponentModel.DataAnnotations;

namespace Warehouse.Model.Models
{
    public class Product
    {
 
        public int Id { get; set; }

        [Required(ErrorMessage = "Product name is required!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Quantity field is required!")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Price field is required!")]
        [RegularExpression(@"((\d)*[,]\d\d)", ErrorMessage = "Price format is 123,45.")]
        public double Price { get; set; }

        [Required(ErrorMessage = "Category is required!")]
        public int CategoryId { get; set; }
        
        public virtual Category Category { get; set; }
    }
}